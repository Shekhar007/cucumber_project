package API_Common_Methods;

import static io.restassured.RestAssured.given;

public class GET_Common_Method

{
	public static int get_statusCode(String endpoint) {
		int statusCode = given().header("Content-Type", "application/json").when().get(endpoint).
				then().extract().statusCode();
		return statusCode;
	}

	public static String get_responseBody(String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").when().get(endpoint).then().extract()
				.response().asString();
		return responseBody;
	}
}