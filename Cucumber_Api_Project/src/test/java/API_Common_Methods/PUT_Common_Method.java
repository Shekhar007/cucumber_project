package API_Common_Methods;

import static io.restassured.RestAssured.given;

import java.net.URI;

public class PUT_Common_Method
{
     public static int put_statusCode(String requestBody, String endpoint)
    {
        int statusCode = given().header("Content-Type","application/json").body(requestBody).when().put(endpoint)
        .then().extract().statusCode(); return statusCode; 
     }
  
  public static String put_responseBody(String requestBody, String endpoint)
    { 
	String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
     .then().extract().response().asString(); return responseBody;
  
    } 
}