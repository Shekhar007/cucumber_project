Feature: Trigger Put API 
@Put_API_TestCases
Scenario Outline: Trigger the put API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in put request body 
	When Send the put request with payload 
	Then Validate put status code 
	And Validate put response body parameters 
	
	Examples: 
	|Name |Job |
	|Anuradha|QA|
	|Daksh|Mgr|
	|Rekha|SrQA|