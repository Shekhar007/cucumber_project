package Data_Driven_step.Definitions;
import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_Common_Methods.POST_Common_Method;
import Endpoint.POST_Endpoint;
import Test_Package.POST_TestCase_1;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Post_data_driven_StepDefinition {
	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;
	static int StatusCode;

	@Given("Enter {string} and {string} in post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) {
		log_dir = Handle_directory.create_log_directory("POST_TestCase_1_logs");
		endpoint = POST_Endpoint.POST_Endpoint_Tc1();
		requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
	}

	@When("Send the post request with payload")
	public void send_the_post_request_with_payload() throws IOException {
		StatusCode = POST_Common_Method.post_statusCode(requestBody, endpoint);
		responseBody = POST_Common_Method.post_responseBody(requestBody, endpoint);
		Handle_api_logs.evidence_creator(log_dir, "POST_TestCase", endpoint, requestBody, responseBody);
		System.out.println(responseBody);
	}

	@Then("Validate post status code")
	public void validate_post_status_code() {
		Assert.assertEquals(StatusCode, 201);
	}

	@Then("Validate post response body parameters")
	public void validate_post_response_body_parameters()   {
		
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		
		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Post response Body Validation Successfully done" +"\n");
	
		
	}

}
