package Data_Driven_step.Definitions;

import java.io.File;

import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import API_Common_Methods.PUT_Common_Method;
import Endpoint.PUT_Endpoint;
import Test_Package.PUT_TestCase_1;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Put_data_driver_StepDefinition {
	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;
	static int StatusCode ;
	
	@Given("Enter {string} and {string} in put request body")
	public void enter_and_in_put_request_body(String req_name, String req_job) {
		log_dir = Handle_directory.create_log_directory("PUT_TestCase_1_logs");
		endpoint = PUT_Endpoint.PUT_Endpoint_Tc1();
		 requestBody = "{\r\n" + "   " + " \"name\": \""+req_name+" \",\r\n" + "  "
				+ "  \"job\": \""+req_job+"\"\r\n" + "}";
		
	   }
	
	@When("Send the put request with payload")
	public void send_the_put_request_with_payload() throws IOException {
		StatusCode = PUT_Common_Method.put_statusCode(requestBody, endpoint);
		responseBody = PUT_Common_Method.put_responseBody(requestBody, endpoint);
		Handle_api_logs.evidence_creator(log_dir, "PUT_TestCase", endpoint, requestBody, responseBody);

		 System.out.println(responseBody);
	
	}
	
	@Then("Validate put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(StatusCode, 200);
	}
	
	@Then("Validate put response body parameters")
	public void validate_put_response_body_parameters()  {
		
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		String req_createdAt = jsp_req.getString("createdAt");
		LocalDateTime currentdate = LocalDateTime.now();

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, req_createdAt);
		System.out.println("Put response Body Validation Successfully done" +"\n");

	}
}
