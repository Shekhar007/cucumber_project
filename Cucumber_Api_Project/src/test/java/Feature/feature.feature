Feature: Trigger  API 
Scenario: Trigger the Post API request with valid request parameters 
	Given Enter NAME and JOB in request body 
	When Send the Post request with payload 
	Then Validate Post status code 
	And Validate Post response body parameters 

	

Scenario: Trigger the Patch API request with valid request parameters 
	Given Enter NAME and JOB in Patch request body 
	When Send the Patch request with payload 
	Then Validate Patch status code 
	And Validate Patch response body parameter 
	
	
		
Scenario: Trigger the Get API 
	Given Valid Get request Base URL 
	When Send the Get request 
	Then Validate Get status code 
	And Validate Get response body parameters 
	
	
Scenario: Trigger the Put API request with valid request parameters 
	Given Enter NAME and JOB in Put request body 
	When Send the Put request with payload 
	Then Validate Put status code 
	And Validate Put response body parameters 
	