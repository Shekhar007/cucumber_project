package Request_Repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_common_methods.Excel_data_extractor;

public class POST_Request_Repository {
	public static String post_request_tc1() throws IOException {
		ArrayList<String> Data = Excel_data_extractor.Excel_data_reader("TestData", "Post_API", "Post_TC1");

		System.out.println(Data);
		String name = Data.get(1);
		String job = Data.get(2);

		String requestbody = "{\r\n" + "    \"name\": \"" + name + "\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";
		return requestbody;
	}

}