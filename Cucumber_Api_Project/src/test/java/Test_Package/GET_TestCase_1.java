package Test_Package;
import java.io.File;

import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_Common_Methods.GET_Common_Method;
import Endpoint.GET_Endpoint;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;

public class GET_TestCase_1 extends GET_Common_Method {
	static  File log_dir;
	static String endpoint;
	static String responseBody;
	
	@BeforeTest
	public static void test_setup() {
	   	 log_dir = Handle_directory.create_log_directory("GET_TestCase_1");
		 endpoint = GET_Endpoint.GET_Endpoint_Tc1();
	}	
	@Test(description = "Validate Response Body")

	public static void Get_Executor() throws IOException {
		
		for (int i = 0; i < 5; i++) {
			int statusCode = get_statusCode(endpoint);
			System.out.println("GET API Triggered");
			System.out.println(statusCode);

			if (statusCode == 200) {
				String responseBody = get_responseBody(endpoint);
				System.out.println(responseBody);
				GET_TestCase_1.validator(endpoint, responseBody);
				break;
			} else {
				System.out.println("Expected statuscode of GET API (200) is not found , hence retrying");
			}
		}
	}
	public static void validator(String endpiont, String responseBody) {
		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		System.out.println(dataarray);

		int id[] = { 1, 2, 3, 4, 5, 6 };
		String email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
		String first_name[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String last_name[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

		int count = dataarray.length();
		System.out.println(count);

		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_first_name = first_name[i];
			String exp_last_name = last_name[i];

			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_email = dataarray.getJSONObject(i).getString("email");
			String res_first_name = dataarray.getJSONObject(i).getString("first_name");
			String res_last_name = dataarray.getJSONObject(i).getString("last_name");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_first_name, exp_first_name);
			Assert.assertEquals(res_last_name, exp_last_name);
		}
		}
		@AfterTest
		public static void Test_Teardown() throws IOException {
			String testclassname = GET_TestCase_1.class.getName();
			Handle_api_logs.evidence_creator(log_dir, testclassname, endpoint, null, responseBody);	
	}
}