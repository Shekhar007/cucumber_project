package Test_Package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import API_Common_Methods.PATCH_Common_Method;
import Endpoint.PATCH_Endpoint;
import Request_Repository.PATCH_Request_Repository;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.restassured.path.json.JsonPath;

public class PATCH_TestCase_1 extends PATCH_Common_Method {
	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest
	public static void test_setup() throws IOException {
		log_dir = Handle_directory.create_log_directory("PATCH_TestCase_1_logs");
		requestBody = PATCH_Request_Repository.PATCH_Request_Repository_Tc1();
		endpoint = PATCH_Endpoint.PATCH_Endpoint_Tc1();
	}
	@Test(description="Validate Response Body")
	public static void Patch_Executor() throws IOException {
		{

			for (int i = 0; i < 5; i++) {
				int statusCode = patch_statusCode(requestBody, endpoint);
				System.out.println("PATCH API Triggered");
				System.out.println(statusCode);

				if (statusCode == 200) {
					String responseBody = patch_responseBody(requestBody, endpoint);
					System.out.println(responseBody);
					PATCH_TestCase_1.validator(requestBody, responseBody);
					break;
				} else {
					System.out.println("Expected statuscode of PATCH API (200) is not found , hence retrying");
				}
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		String req_createdAt = jsp_req.getString("createdAt");
		LocalDateTime currentdate = LocalDateTime.now();

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, req_createdAt);
	}
	@AfterTest
	public static void Test_Teardown() throws IOException {
		String testclassname = PATCH_TestCase_1.class.getName();
		Handle_api_logs.evidence_creator(log_dir, testclassname, endpoint, requestBody, responseBody);

	}

}

