package Utility_common_methods;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;

public class Handle_api_logs {
	public static void evidence_creator(File dir_name, String Filename, String endpoint, String requestBody, String responseBody)throws IOException {
	
		File newFile = new File(dir_name + "\\" +Filename+ ".txt");
		System.out.println("To save request & response body we've created new file name :" + newFile.getName());
		
		FileWriter dataWriter = new FileWriter(newFile);
		dataWriter.write("Request body is :" + requestBody + "\n\n");
		dataWriter.write("End point is :" + endpoint + "\n\n");
		dataWriter.write("Response body is :" + responseBody);

		dataWriter.close();
		
		
	}
    
}