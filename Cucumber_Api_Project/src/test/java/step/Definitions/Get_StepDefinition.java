package step.Definitions;

import java.io.File;

import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import API_Common_Methods.GET_Common_Method;
import Endpoint.GET_Endpoint;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Get_StepDefinition {
    static String endpoint;
    static int StatusCode;
    static String responseBody;
    static File log_dir;
    
   
     @Given("Valid Get request Base URL")
     public void valid_get_request_base_url() {
    	endpoint = GET_Endpoint.GET_Endpoint_Tc1();
    	log_dir = Handle_directory.create_log_directory("GET_TestCase_1_logs");
         // throw new io.cucumber.java.PendingException();
     }
     
     @When("Send the Get request")
     public void send_the_get_request() throws IOException {
    	 StatusCode =GET_Common_Method.get_statusCode( endpoint);
		 responseBody =GET_Common_Method.get_responseBody(endpoint);
		 System.out.println(responseBody);
			Handle_api_logs.evidence_creator(log_dir, "GET_TestCase_1_logs",endpoint,null,responseBody);	 
        // throw new io.cucumber.java.PendingException();
     }
     
     @Then("Validate Get status code")
     public void validate_get_status_code() {
    	 Assert.assertEquals(StatusCode,200);
    	 //throw new io.cucumber.java.PendingException();
     }
     
     @Then("Validate Get response body parameters")
     public void validate_get_response_body_parameters() {
    	 JSONObject array_res = new JSONObject(responseBody);
 		JSONArray dataarray = array_res.getJSONArray("data");
 		System.out.println(dataarray);

 		int id[] = { 1, 2, 3, 4, 5, 6 };
 		String email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
 				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };
 		String first_name[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
 		String last_name[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };

 		int count = dataarray.length();
 		System.out.println(count);

 		for (int i = 0; i < count; i++) {
 			int exp_id = id[i];
 			String exp_email = email[i];
 			String exp_first_name = first_name[i];
 			String exp_last_name = last_name[i];

 			int res_id = dataarray.getJSONObject(i).getInt("id");
 			String res_email = dataarray.getJSONObject(i).getString("email");
 			String res_first_name = dataarray.getJSONObject(i).getString("first_name");
 			String res_last_name = dataarray.getJSONObject(i).getString("last_name");

 			Assert.assertEquals(res_id, exp_id);
 			Assert.assertEquals(res_email, exp_email);
 			Assert.assertEquals(res_first_name, exp_first_name);
 			Assert.assertEquals(res_last_name, exp_last_name);
    	// throw new io.cucumber.java.PendingException();
 			
     }
			System.out.println("Get response Body Validation Successfully done");

     }	
    
}

