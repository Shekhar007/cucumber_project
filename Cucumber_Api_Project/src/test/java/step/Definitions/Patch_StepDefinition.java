package step.Definitions;
import java.io.File;

import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import API_Common_Methods.PATCH_Common_Method;
import Endpoint.PATCH_Endpoint;
import Request_Repository.PATCH_Request_Repository;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Patch_StepDefinition extends PATCH_Common_Method {

	static String requestBody;
	static int StatusCode;
	static String responseBody;
	static String endpoint;
	static File log_dir;

	@Before
	public void test_setup() {
		System.out.println("Triggering the API process is started");
	}

	@Given("Enter NAME and JOB in Patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		log_dir = Handle_directory.create_log_directory("PATCH_TestCase_1_logs");
		requestBody = PATCH_Request_Repository.PATCH_Request_Repository_Tc1();
		endpoint = PATCH_Endpoint.PATCH_Endpoint_Tc1();
	}

	@When("Send the Patch request with payload")
	public void send_the_patch_request_with_payload() throws IOException {
		StatusCode = PATCH_Common_Method.patch_statusCode(requestBody, endpoint);
		responseBody = PATCH_Common_Method.patch_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		Handle_api_logs.evidence_creator(log_dir, "PATCH_TestCase_1_logs", endpoint, requestBody, responseBody);

	}

	@Then("Validate Patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(StatusCode, 200);
	}

	@Then("Validate Patch response body parameter")
	public void validate_patch_response_body_parameter() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		String req_createdAt = jsp_req.getString("createdAt");
		LocalDateTime currentdate = LocalDateTime.now();

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdAt = jsp_res.getString("createdAt");

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, req_createdAt);
		System.out.println("Patch response Body Validation Successfully done");

	}

	@After
	public void teardown() {
		System.out.println("Triggering the API process is ended \n");
	}
}
