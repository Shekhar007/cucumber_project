Feature: Trigger Patch API 
@Patch_API_TestCases
Scenario Outline: Trigger the patch API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in patch request body 
	When Send the patch request with payload 
	Then Validate patch status code 
	And Validate patch response body parameters 
	
Examples: 
	|Name |Job |
	|Anuradha|Mgr|
	|Radhika|QA|
	|Krishna|SrQA|