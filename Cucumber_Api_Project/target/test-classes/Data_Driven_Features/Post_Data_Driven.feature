Feature: Trigger Post API 
@Post_API_TestCases
Scenario Outline: Trigger the post API request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in post request body 
	When Send the post request with payload 
	Then Validate post status code 
	And Validate post response body parameters 
	
	Examples: 
	|Name |Job |
	|Pankaj|QA|
	|Dhanshree|Mgr|
	|Anuradha|SrQA|